﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Sol_ProductUser.Models.Person.Employee;
using Sol_ProductUser.Models.Person;
using Sol_ProductUser.DAL.Employee;

namespace UTP.ModuleTest.Employee
{
    [TestClass]
    public class EmployeeUnitTest
    {
        [TestMethod]
        public void InsertEmployeeTestMethod()
        {
            Task.Run(async () =>
            {

                var employeeEntityObj = new EmployeeEntity()
                {
                    Person = new PersonEntity()
                    {
                        FirstName = "Ram",
                        LastName = "Shinde",
                        Login = new LoginEntity()
                        {
                            UserName = "ram",
                            Password = "1111"
                        },
                        Communication = new CommunicationEntity()
                        {
                            MobileNo = "9842036578",
                            EmailId = "ram@gmail.com"
                        }
                    }
                };

                var result = await new EmployeeDal()?.InsertEployeeData(employeeEntityObj);

                Assert.IsNotNull(result);

            }).Wait();
        }
    }
}
