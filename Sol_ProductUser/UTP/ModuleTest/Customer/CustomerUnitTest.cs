﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Sol_ProductUser.Models.Person.Customer;
using Sol_ProductUser.Models.Person;
using Sol_ProductUser.DAL.Customer;

namespace UTP
{
    [TestClass]
    public class CustomerUnitTest
    {
        [TestMethod]
        public void InsertCustomerTestMethod()
        {
            Task.Run(async () =>
            {

                var customerEntityObj = new CustomerEntity()
                {
                    Person =new PersonEntity()
                    {
                        FirstName = "Ravina",
                        LastName = "Bhosale",
                        Login = new LoginEntity()
                        {
                            UserName = "ravina",
                            Password = "1234"
                        },
                        Communication = new CommunicationEntity()
                        {
                            MobileNo = "9870789865",
                            EmailId = "ravina@yahoo.com"
                        }
                    }
                };

                var result = await new CustomerDal()?.InsertCustomerData(customerEntityObj);

                Assert.IsNotNull(result);

            }).Wait();
        }
    }
}
