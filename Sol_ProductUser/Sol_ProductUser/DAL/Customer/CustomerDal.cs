﻿using Sol_ProductUser.DAL.ORD.Customer;
using Sol_ProductUser.Models.Person.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ProductUser.DAL.Customer
{
    public class CustomerDal
    {
        #region Declaration
        private CustomerDcDataContext dbObj = null;
        #endregion

        #region Constructor
        public CustomerDal()
        {
            dbObj = new CustomerDcDataContext();
        }
        #endregion

        #region Public Method
        public dynamic InsertCustomerData(CustomerEntity customerEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                var setQuery =
               dbObj
               ?.uspSetCustomer
               (
                   "Add",
                   customerEntityObj?.Person?.PersonId ?? 0,
                   customerEntityObj?.Person?.FirstName ?? null,
                   customerEntityObj?.Person?.LastName ?? null,
                   customerEntityObj?.Person?.Communication?.MobileNo ?? null,
                   customerEntityObj?.Person?.Communication?.EmailId ?? null,
                   customerEntityObj?.Person?.Login?.UserName ?? null,
                   customerEntityObj?.Person?.Login?.Password ?? null,
                   ref status,
                   ref message
                   );

                return (status == 1) ? true : false;
            }
            catch(Exception)
            {
                throw;
            }
           
        }

        #endregion
    }
}