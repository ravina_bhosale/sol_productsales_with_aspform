﻿using Sol_ProductUser.DAL.ORD.Employee;
using Sol_ProductUser.Models.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ProductUser.DAL.Employee
{
    public class EmployeeDal
    {
        #region Declaration
        private EmployeeDcDataContext dbObj = null;
        #endregion

        #region Constructor
        public EmployeeDal()
        {
            dbObj = new EmployeeDcDataContext();
        }
        #endregion

        #region Public Method
        public dynamic InsertEployeeData(EmployeeEntity employeeEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                var setQuery =
               dbObj
               ?.uspSetEmployee
               (
                   "Add",
                   employeeEntityObj?.Person?.PersonId ?? 0,
                   employeeEntityObj?.Person?.FirstName ?? null,
                   employeeEntityObj?.Person?.LastName ?? null,
                   employeeEntityObj?.Person?.Communication?.MobileNo ?? null,
                   employeeEntityObj?.Person?.Communication?.EmailId ?? null,
                   employeeEntityObj?.Person?.Login?.UserName ?? null,
                   employeeEntityObj?.Person?.Login?.Password ?? null,
                   ref status,
                   ref message
                   );

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion
    }
}