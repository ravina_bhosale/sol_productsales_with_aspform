﻿using Newtonsoft.Json;
using Sol_ProductUser.DAL.Employee;
using Sol_ProductUser.Models.Person;
using Sol_ProductUser.Models.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ProductUser
{
    public partial class EmployeeRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // Mapping
                var employeeEntityObj = this.MapCustomerData();


                var result = new EmployeeDal().InsertEployeeData(employeeEntityObj);

                // Bind and Page Redirection
                this.Bind(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private EmployeeEntity MapCustomerData()
        {
            try
            {
                // Create an instance of Customer Entity and Map control data into Poco
                EmployeeEntity employeeEntityObj = new EmployeeEntity()
                {
                    Person = new PersonEntity()
                    {
                        FirstName = txtFirstName.Text,
                        LastName = txtLastName.Text,
                        Communication = new CommunicationEntity()
                        {
                            MobileNo = txtMobileNo.Text,
                            EmailId = txtEmailId.Text
                        },
                        Login = new LoginEntity()
                        {
                            UserName = txtUserName.Text,
                            Password = txtPassword.Text
                        },
                    }
                };

                return employeeEntityObj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Bind(dynamic result)
        {
            try
            {
                if (result == true)
                {
                    string message = "Data sucessfully stored";

                    string script = string.Format("OnMessageDisplay('{0}')", message);


                    ScriptManager.RegisterStartupScript
                       (
                           this,
                           this.GetType(),
                           "DisplayMessageScript",
                           script,
                           true
                       );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}