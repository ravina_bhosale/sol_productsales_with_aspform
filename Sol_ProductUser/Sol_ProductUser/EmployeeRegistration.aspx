﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeRegistration.aspx.cs" Inherits="Sol_ProductUser.EmployeeRegistration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Employee Registation</title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"  />

     <link type="text/css" href="CSS/MainStyleSheet.css" rel="stylesheet" />


     <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>

    <script type="text/javascript">

            function OnMessageDisplay(message)
            {

                $(document).ready(function () {


                    $("#lblMessage").text(message);

                    $("#divMessagePanel")
                        .show()
                        .fadeOut(5000, function () {
                            OnTextBoxClear();
                        });

                });
        }

            function OnTextBoxClear() {
                    // Clear TextBox
                     $("#<%=txtFirstName.ClientID %>").val("");
                     $("#<%=txtLastName.ClientID %>").val("");
                     $("#<%=txtMobileNo.ClientID %>").val("");
                     $("#<%=txtLastName.ClientID %>").val("");
                     $("#<%=txtEmailId.ClientID %>").val("");
                     $("#<%=txtUserName.ClientID %>").val("");
                    $("#<%=txtPassword.ClientID %>").val("");

                    // Focus on First Name
                    $("#<%=txtFirstName.ClientID %>").focus();
        }



    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>

         <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>


         <table class="tbl">
            <tr>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" placeHolder="First Name" CssClass="txtbox"></asp:TextBox>
                </td>
                </tr>

               <tr>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" placeHolder="Last Name" CssClass="txtbox"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <asp:TextBox ID="txtMobileNo" runat="server" placeHolder="MobileNo." CssClass="txtbox"></asp:TextBox>
                </td>
            </tr>

              <tr>
                <td>
                    <asp:TextBox ID="txtEmailId" runat="server" placeHolder="EmailID" CssClass="txtbox"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" placeHolder="UserName" CssClass="txtbox"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" placeHolder="Password" CssClass="txtbox"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    
                    <asp:Button ID="btnSave" Text="Save" runat="server" OnClick="btnSave_Click" CssClass="button" Width="68px"></asp:Button>
                </td>
            </tr>
            <tr>
                 <td>
                      <div id="divMessagePanel" class="w3-panel w3-blue w3-card-4">
                            <span id="lblMessage"></span>
                        </div>

                 </td>
            </tr>
        </table>

              </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
