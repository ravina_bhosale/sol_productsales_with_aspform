﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ProductUser.Models.Person
{
    public class CommunicationEntity
    {
        public decimal? PersonId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}