﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ProductUser.Models.Person.Customer
{
    public class CustomerEntity
    {
        public decimal? CustomerId { get; set; }

        public PersonEntity Person { get; set; }
    }
}