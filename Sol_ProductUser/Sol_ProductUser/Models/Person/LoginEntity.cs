﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_ProductUser.Models.Person
{
    public class LoginEntity
    {
        public decimal? PersonId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
