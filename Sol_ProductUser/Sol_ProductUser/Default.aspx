﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_ProductUser.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        
       .tbl
        {
            width : 50%;
            margin:auto;
            border-collapse:collapse;
        }
        .button
        {
            background-color:burlywood;
            color:red;
            border-radius:20px;
            width:30%;
            height:40px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table  class="tbl" >
            <tr>
                <td>
                    <asp:Button ID="btnCustomerReg" Text="Customer Registration" runat="server" CssClass="button" OnClick="btnCustomerReg_Click"/>
                
                    <asp:Button ID="btnEmployeeReg" Text="Employee Registration" runat="server" CssClass="button" OnClick="btnEmployeeReg_Click" />
                </td>
                
            </tr>

        </table>
        
    </div>
    </form>
</body>
</html>
