﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ProductUser
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEmployeeReg_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmployeeRegistration.aspx");
        }

        protected void btnCustomerReg_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CustomerRegistration.aspx");
        }
    }
}